﻿using EmpresasApi.Models.Context;
using EmpresasApi.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace EmpresasApi.Controllers
{
    
    public class EnterprisesController : ApiController
    {
        private DataBaseContext db = new DataBaseContext();

        public EnterprisesController(DataBaseContext dbContext)
        {
            this.db = dbContext;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var empresas = db.EmpresaContext;
            return Ok(empresas);
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            var result = await db.Set<Empresa>().FindAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult Get(int enterprise_types, string name)
        {
            if (enterprise_types <= 0)
                return BadRequest();

            if (string.IsNullOrEmpty(name))
                return BadRequest();

            var result = db.Set<Empresa>().Where(x => x.EnterpriseType.Id == enterprise_types && x.EnterpriseName.ToUpper().Contains(name.ToUpper())).ToList();

            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}
