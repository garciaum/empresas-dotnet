﻿namespace EmpresasApi.Models.Entities
{
    public class TipoEmpresa
    {
        public int Id { get; set; }
        public string EnterpriseTypeName { get; set; }
    }
}