﻿using EmpresasApi.Models.Entities;
using System.Data.Entity;

namespace EmpresasApi.Models.Context
{
    public class DataBaseContext : DbContext
    {
        public DbSet<Empresa> EmpresaContext { get; set; }
    
        public DataBaseContext() : base("DataBaseContext")
        {

        }
    }
}